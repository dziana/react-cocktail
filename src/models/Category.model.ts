export type CategoryModel = {
    strCategory: string;
}

export type CategoryListModel = {
    drinks: CategoryModel[];
}
