export type Size =
    'small' | 'big' | 'mini' | 'tiny' | 'medium' | 'large' | 'huge' | 'massive';
