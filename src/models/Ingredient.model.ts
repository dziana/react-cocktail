export type IngredientDetailModel = {
    idIngredient: string;
    strIngredient: string;
    strDescription: string;
    strType: string;
    strAlcohol: string;
    strABV: string;
}

export type IngredientDetailListModel = {
    ingredients: IngredientDetailModel[];
}

export type IngredientModel = {
    strIngredient1: string;
}

export type IngredientListModel = {
    drinks: IngredientModel[];
}
