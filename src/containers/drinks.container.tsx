import React from 'react';
import styled from "styled-components";
import {AxiosResponse} from "axios";

import {useData} from "../hooks/useData";
import {ApiService} from "../services/apiService";
import Spinner from "../components/common/Spinner/Spinner";
import ErrorIndicator from "../components/common/ErrorIndicator";
import {CategoryListModel} from "../models/Category.model";
import withApiService from "../components/wrapper/withApiService";
import CategoryCardGroup from "../components/content/CategoryCardGroup";
import SearchItems from "../components/content/SearchItems";
import {CocktailDetailModel} from "../models/Cocktail.model";
import DrinkDetailItem from "../components/content/DrinkDetailItem";

type DrinksContainerProps = {
    apiService: ApiService
}

const Wrapper = styled.div`
  padding: 10px;
`;

const SearchItemsWrapper = styled.div`
  margin-bottom: 20px !important;
`


const DrinksContainer = ({apiService}: DrinksContainerProps) => {
    const state = useData(apiService.getAllCategories)

    const getItemsCocktail = (name: string) => {
        return apiService.getCocktailByName(name);
    }

    const getDataCocktail = (data: AxiosResponse<any>) => {
        return data.data.drinks;
    }

    const renderItemsCocktail = (data: CocktailDetailModel) => {
        return <DrinkDetailItem key={data.idDrink} drink={data} imageSize="tiny"/>
    }

    return (
        <div>
            {state.loading && <Spinner/>}
            {state.error && <ErrorIndicator/>}
            {state.data &&
            <Wrapper>
                <SearchItemsWrapper>
                    <SearchItems getItems={getItemsCocktail}
                                 getData={getDataCocktail}
                                 renderItems={renderItemsCocktail}
                    />
                </SearchItemsWrapper>
                <CategoryCardGroup
                    categoryList={(state?.data! as CategoryListModel).drinks}
                />
            </Wrapper>
            }
        </div>
    );
};

export default withApiService(DrinksContainer);
