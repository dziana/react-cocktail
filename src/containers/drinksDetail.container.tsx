import React, {useCallback} from 'react';
import {useParams} from "react-router-dom";
import {Item, Segment} from "semantic-ui-react";

import {useData} from "../hooks/useData";
import withApiService from "../components/wrapper/withApiService";
import {ApiService} from "../services/apiService";
import Spinner from "../components/common/Spinner/Spinner";
import ErrorIndicator from "../components/common/ErrorIndicator";
import {CocktailDetailListModel} from "../models/Cocktail.model";
import DrinkDetailItem from "../components/content/DrinkDetailItem";
import DrinkDetailIngredientList from "../components/content/DrinkDetailIngredientList";
import styled from "styled-components";

type DrinksDetailContainerProps = {
    apiService: ApiService
}

type DrinkDetailProps = {
    data: CocktailDetailListModel
}

type ParamTypes = {
    name: string,
    id: string
}

const Wrapper = styled.div`
  padding: 10px
`;

const DrinkDetail = ({data}: DrinkDetailProps) => {
    const drink = data?.drinks[0];
    return (
        <React.Fragment>
            <Segment><Item.Group><DrinkDetailItem drink={drink}/></Item.Group></Segment>
            <DrinkDetailIngredientList drink={drink}/>
        </React.Fragment>
    );
};

const DrinksDetailContainer = ({apiService}: DrinksDetailContainerProps) => {
    const {id} = useParams<ParamTypes>();
    const memoizedCallback = useCallback(
        () => {
            return apiService.getCocktailById(id)
        },
        [id, apiService],
    );
    const state = useData(memoizedCallback)
    return (
        <Wrapper>
            {state.loading && <Spinner/>}
            {state.error && <ErrorIndicator/>}
            {state.data && <DrinkDetail data={state.data!}/>}
        </Wrapper>
    );
};

export default withApiService(DrinksDetailContainer);
