import React from 'react';
import styled from "styled-components";

import {Container, Header} from "semantic-ui-react";
import CocktailImage from "../components/content/CocktailImage";

const ImageWrapper = styled.div`
  display: flex;
  justify-content: center;
`

const HomepageHeading = () => (
    <Container text>
        <Header
            as='h1'
            content='Make your perfect cocktail'
            inverted
            style={{
                fontSize: '4em',
                fontWeight: 'normal',
                marginBottom: 0,
                marginTop: '3em',
            }}
        />
        <Header
            as='h2'
            content='Do whatever you want when you want to.'
            inverted
            style={{
                fontSize: '1.7em',
                fontWeight: 'normal',
                marginTop: '1.5em',
            }}
        />
    </Container>
)

const HomeContainer = () => {
    return (
        <div>
            <HomepageHeading/>
            <ImageWrapper>
                <CocktailImage/>
            </ImageWrapper>
        </div>
    );
}

export default HomeContainer;
