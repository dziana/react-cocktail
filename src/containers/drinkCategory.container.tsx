import React, {useCallback} from 'react';
import {useParams} from 'react-router-dom';

import {useData} from "../hooks/useData";
import withApiService from "../components/wrapper/withApiService";
import {ApiService} from "../services/apiService";
import Spinner from "../components/common/Spinner/Spinner";
import ErrorIndicator from "../components/common/ErrorIndicator";
import {CocktailListModel} from "../models/Cocktail.model";
import CocktailCardGroup from "../components/content/CocktailCardGroup";
import styled from "styled-components";

const Wrapper = styled.div`
  padding: 10px;
`;

type DrinkCategoryContainerProps = {
    apiService: ApiService
}

type ParamTypes = {
    name: string
}

const DrinkCategoryContainer = ({apiService}: DrinkCategoryContainerProps) => {
    const {name} = useParams<ParamTypes>();
    const category = name.replaceAll('__', '_/_');
    const memoizedCallback = useCallback(
        () => {
            return apiService.getAllCocktailsByCategory(category)
        },
        [category, apiService],
    );
    const state = useData(memoizedCallback)
    return (
        <Wrapper>
            {state.loading && <Spinner/>}
            {state.error && <ErrorIndicator/>}
            {state.data && <CocktailCardGroup cocktailList={(state?.data! as CocktailListModel).drinks}/>
            }
        </Wrapper>
    );
};

export default withApiService(DrinkCategoryContainer);
