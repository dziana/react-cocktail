import React from 'react';
import styled from "styled-components";
import {AxiosResponse} from "axios";

import {useData} from "../hooks/useData";
import withApiService from "../components/wrapper/withApiService";
import {ApiService} from "../services/apiService";
import Spinner from "../components/common/Spinner/Spinner";
import ErrorIndicator from "../components/common/ErrorIndicator";
import {IngredientDetailModel, IngredientListModel} from "../models/Ingredient.model";
import IngredientList from "../components/content/IngredientList";
import SearchItems from "../components/content/SearchItems";
import IngredientDetailItem from "../components/content/IngredientDetailItem";

type IngredientsContainerProps = {
    apiService: ApiService
}

const Wrapper = styled.div`
  padding: 10px;
`;

const SearchItemsContainer = styled.div`
  margin-bottom: 20px;
`;

const IngredientsContainer = ({apiService}: IngredientsContainerProps) => {
    const state = useData(apiService.getAllIngredients)

    const getItemsIngredient = (name: string) => {
        return apiService.getIngredientByName(name);
    }

    const getDataIngredient = (data: AxiosResponse<any>) => {
        return data.data.ingredients;
    }

    const renderItemsIngredient = (data: IngredientDetailModel) => {
        return <IngredientDetailItem key={data.idIngredient} data={data} imageSize="tiny"/>
    }

    return (
        <div>
            {state.loading && <Spinner/>}
            {state.error && <ErrorIndicator/>}
            {state.data &&
            <Wrapper>
                <SearchItemsContainer>
                    <SearchItems getItems={getItemsIngredient}
                                 getData={getDataIngredient}
                                 renderItems={renderItemsIngredient}
                    />
                </SearchItemsContainer>

                <IngredientList
                    ingredientList={(state?.data! as IngredientListModel).drinks}
                />

            </Wrapper>
            }
        </div>
    );
};

export default withApiService(IngredientsContainer);
