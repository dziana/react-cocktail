import axios from "axios";

import {CategoryListModel} from "../models/Category.model";
import {IngredientListModel, IngredientDetailListModel} from "../models/Ingredient.model";
import {CocktailDetailListModel, CocktailListModel} from "../models/Cocktail.model";

const instance = axios.create({
    baseURL: 'https://www.thecocktaildb.com/api/json/v1/1/'
});

export class ApiService {
    getAllCategories = async () =>
        await instance.get<CategoryListModel>(`list.php?c=list`);

    getAllIngredients = async () =>
        await instance.get<IngredientListModel>(`list.php?i=list`);

    getAllCocktailsByCategory = async (categoryName: string) =>
        await instance.get<CocktailListModel>
        (`filter.php?c=${categoryName}`);

    getCocktailByName = async (name: string) =>
        await instance.get<CocktailDetailListModel>
        (`search.php?s=${name}`);

    getIngredientByName = async (name: string) =>
        await instance.get<IngredientDetailListModel>
        (`search.php?i=${name}`);

    getCocktailById = async (id: string) =>
        await instance.get<CocktailDetailListModel>
        (`lookup.php?i=${id}`);

    getIngredientById = async (id: string) =>
        await instance.get<IngredientDetailListModel>
        (`lookup.php?iid=${id}`);

    getRandomCocktail = async () =>
        await instance.get<CocktailDetailListModel>(`random.php`);

}
