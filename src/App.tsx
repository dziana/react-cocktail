import React from 'react';
import {BrowserRouter, Switch, Route} from "react-router-dom";
import 'semantic-ui-css/semantic.min.css'

import ErrorBoundary from "./components/common/ErrorBoundary";
import Header from "./components/common/Header";
import HomeContainer from "./containers/home.container";
import DrinksContainer from "./containers/drinks.container";
import IngredientsContainer from "./containers/ingredients.container";
import DrinkCategoryContainer from "./containers/drinkCategory.container";
import DrinksDetailContainer from "./containers/drinksDetail.container";

function App() {
    return (
        <ErrorBoundary>
            <BrowserRouter>
                <Header/>
                <Switch>
                    <Route path="/" exact component={HomeContainer}/>
                    <Route path="/drinks" exact component={DrinksContainer}/>
                    <Route path="/drinks/:name/" exact component={DrinkCategoryContainer}/>
                    <Route path="/drinks/:name/:id" component={DrinksDetailContainer}/>
                    <Route path="/ingredients" component={IngredientsContainer}/>
                    <Route render={() => (<h2>Page not found</h2>)}/>
                </Switch>
            </BrowserRouter>
        </ErrorBoundary>
    );
}

export default App;
