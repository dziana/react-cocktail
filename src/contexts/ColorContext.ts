import React from "react";

export const ColorContext = React.createContext(['red', 'orange', 'yellow', 'olive', 'green', 'teal',
    'blue', 'violet', 'purple', 'pink', 'brown', 'grey']);
