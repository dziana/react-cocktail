import React from "react";

import {ApiService} from "../services/apiService";

export const ApiContext = React.createContext(new ApiService());
