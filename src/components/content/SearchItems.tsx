import React, {useState} from 'react';
import {Item, Segment} from "semantic-ui-react";
import {CocktailDetailModel} from "../../models/Cocktail.model";
import {IngredientDetailModel} from "../../models/Ingredient.model";
import {AxiosResponse} from "axios";
import SearchForm from "../forms/SearchForm";
import styled from "styled-components";

type SearchItemsProps = {
    getItems: (name: string) => Promise<AxiosResponse<any>>
    getData: (data: AxiosResponse<any>) => IngredientDetailModel[] | CocktailDetailModel[] | null;
    renderItems: (data: any) => JSX.Element
}

type SearchState = {
    loading: boolean;
    error: boolean;
    data: IngredientDetailModel[] | CocktailDetailModel[] | null;
}

const SearchDataContainer = styled.div`
  margin-top: 20px;
  max-height: calc(50vh - 100px);
  overflow: auto;
  overflow-x: hidden;
`;

const SearchItems = ({getItems, getData, renderItems}: SearchItemsProps) => {
    const [searchState, setSearchState] = useState<SearchState>({
        loading: false,
        error: false,
        data: null
    });
    const onHandleSubmit = (name: string) => {
        setSearchState({
            loading: true,
            error: false,
            data: null
        });
        getItems(name)
            .then((data) => {
                setSearchState({
                    loading: false,
                    error: false,
                    data: getData(data)
                });
            })
            .catch(() => {
                setSearchState({
                    loading: false,
                    error: true,
                    data: null
                });
            })
    }

    return (
        <div>
            <SearchForm onHandleSubmit={onHandleSubmit} loading={searchState.loading} error={searchState.error}/>
            {searchState.data && (
                <SearchDataContainer>
                    <Segment>
                        <Item.Group divided>
                            {(searchState.data! as any[]).map(e => (
                                renderItems(e)
                            ))}
                        </Item.Group>
                    </Segment>
                </SearchDataContainer>
            )}

        </div>
    );
}

export default SearchItems;
