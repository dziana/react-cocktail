import {Item} from "semantic-ui-react";
import React from "react";

import {CocktailDetailModel} from "../../models/Cocktail.model";
import {Size} from "../../models/Size.model";

type DrinkDetailItemProps = {
    drink: CocktailDetailModel,
    imageSize?: Size;
}

const DrinkDetailItem = ({drink, imageSize = 'small'}: DrinkDetailItemProps) => {
    const {strDrink, strDrinkThumb, strInstructions, strAlcoholic, strCategory} = drink;
    return (
        <Item>
            <Item.Image size={imageSize} src={strDrinkThumb}/>

            <Item.Content>
                <Item.Header>{strDrink}</Item.Header>
                <Item.Meta>
                    <span>{strAlcoholic}</span>
                    <span>{strCategory}</span>
                </Item.Meta>
                <Item.Description>{strInstructions}</Item.Description>
            </Item.Content>
        </Item>
    );
}

export default DrinkDetailItem;
