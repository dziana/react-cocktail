import {Icon, List} from "semantic-ui-react";
import React from "react";
import {CocktailDetailModel} from "../../models/Cocktail.model";
import {Color} from "../../models/Color.model";
import withColors from "../wrapper/withColors";


type DrinkDetailIngredientListProps = {
    drink: CocktailDetailModel,
    colors: Color[]
}

type Ingredient = {
    name: string;
    amount: string;
    id: number;
}

const prepareIngredientList = (item: any) => {
    let array: Ingredient[] = [];
    for (let i = 1; i <= 15; i++) {
        if (item[`strIngredient${i}`]) {
            const ingredient = {
                name: item[`strIngredient${i}`],
                amount: item[`strMeasure${i}`],
                id: i
            }
            array = [...array, ingredient];
        }
    }
    return array;
}

const DrinkDetailIngredientList = ({drink, colors}: DrinkDetailIngredientListProps) => {
    const ingredientList = prepareIngredientList(drink);
    return (
        <List celled inverted>
            {ingredientList.map((e: Ingredient, i: number) => (
                <List.Item key={e.id}>
                    <Icon name='arrow right' color={colors[(i % colors.length)]}/>
                    <List.Content>
                        <List.Header>{e.name}</List.Header>
                        {e.amount}
                    </List.Content>
                </List.Item>
            ))}
        </List>
    );
}

export default withColors(DrinkDetailIngredientList);
