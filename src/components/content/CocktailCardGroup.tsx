import {useHistory} from "react-router-dom";
import {Card} from "semantic-ui-react";
import React from "react";

import {CocktailModel} from "../../models/Cocktail.model";

type CocktailCardGroupProps = {
    cocktailList: CocktailModel[]
}

const CocktailCardGroup = ({cocktailList}: CocktailCardGroupProps) => {
    const history = useHistory();
    return (
        <Card.Group itemsPerRow={4}>
            {
                cocktailList?.map(
                    (e: CocktailModel) => (
                        <Card
                            key={e.idDrink}
                            image={e.strDrinkThumb}
                            header={e.strDrink}
                            onClick={() => history.push(e.idDrink)}
                        />
                    )
                )}
        </Card.Group>
    )
}

export default CocktailCardGroup;
