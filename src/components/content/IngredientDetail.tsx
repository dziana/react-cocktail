import React, {useCallback} from 'react';
import {Item, Loader} from "semantic-ui-react";

import {ApiService} from "../../services/apiService";
import {useData} from "../../hooks/useData";
import ErrorIndicator from "../common/ErrorIndicator";
import {IngredientDetailListModel} from "../../models/Ingredient.model";
import withApiService from "../wrapper/withApiService";
import IngredientDetailItem from "./IngredientDetailItem";

type IngredientsDetailAccContentProps = {
    name: string,
    apiService: ApiService
}

const IngredientsDetailAccContent = ({name, apiService}: IngredientsDetailAccContentProps) => {
    const memoizedCallback = useCallback(
        () => {
            return apiService.getIngredientByName(name)
        },
        [name, apiService],
    );
    const state = useData(memoizedCallback);
    return (
        <div>
            {state.loading && <Loader active inline='centered'/>}
            {state.error && <ErrorIndicator/>}
            {state.data && (
                <Item.Group>
                    <IngredientDetailItem data={(state?.data! as IngredientDetailListModel).ingredients[0]}/>
                </Item.Group>
            )
            }
        </div>
    );
}
;

export default withApiService(IngredientsDetailAccContent);
