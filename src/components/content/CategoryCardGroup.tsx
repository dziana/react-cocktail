import {useHistory} from "react-router-dom";
import {Card} from "semantic-ui-react";
import React from "react";

import {CategoryModel} from "../../models/Category.model";
import {Color} from "../../models/Color.model";
import withColors from "../wrapper/withColors";

type CategoryCardProps = {
    categoryList: CategoryModel[]
    colors: Color[]
}

const CategoryCardGroup = ({categoryList, colors}: CategoryCardProps) => {
    const history = useHistory();
    return (
        <Card.Group itemsPerRow={4}>
            {
                categoryList?.map(
                    (e: CategoryModel, i: number) => {
                        const linkCategory = e.strCategory
                            .replaceAll(/\s+/g, '_')
                            .replaceAll(/\//g, '')
                        return (
                            <Card
                                key={i}
                                color={colors[(i % colors.length)]}
                                header={e.strCategory}
                                onClick={() => history.push(`/drinks/${linkCategory}/`)}
                            />
                        );
                    }
                )}
        </Card.Group>
    );
}

export default withColors(CategoryCardGroup);
