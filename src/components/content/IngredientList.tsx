import {Accordion, Icon} from "semantic-ui-react";
import React, {useState} from "react";

import {IngredientModel} from "../../models/Ingredient.model";
import withColors from "../wrapper/withColors";
import {Color} from "../../models/Color.model";
import IngredientsDetailAccContent from "./IngredientDetail";

type IngredientListProps = {
    ingredientList: IngredientModel[];
    colors: Color[]
}

const IngredientList = ({ingredientList, colors}: IngredientListProps) => {
    const [activeIndex, setActiveIndex] = useState(-1);

    return (
        <Accordion fluid styled>
            {
                ingredientList.map((e: IngredientModel, i: number) =>
                    (
                        <React.Fragment key={i}>
                            <Accordion.Title
                                active={activeIndex === i}
                                index={i}
                                onClick={() => {
                                    setActiveIndex((prevState => i === prevState ? -1 : i))
                                }}
                            >
                                <Icon color={colors[(i % colors.length)]} name='dropdown'/>
                                {e.strIngredient1}
                            </Accordion.Title>
                            {
                                activeIndex === i && (
                                    <Accordion.Content active={true}>
                                        <IngredientsDetailAccContent name={e.strIngredient1}/>
                                    </Accordion.Content>
                                )
                            }
                        </React.Fragment>
                    )
                )
            }
        </Accordion>
    );
}

export default withColors(IngredientList);
