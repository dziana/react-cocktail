import {IngredientDetailModel} from "../../models/Ingredient.model";
import {Item} from "semantic-ui-react";
import React from "react";
import {Size} from "../../models/Size.model";

type IngredientDetailProps = {
    data: IngredientDetailModel,
    imageSize?: Size;
}

const IngredientDetailItem = ({data, imageSize = 'small'}: IngredientDetailProps) => {
    const {strIngredient, strDescription, strType, strAlcohol, strABV} = data;
    const imageSrc = `https://www.thecocktaildb.com/images/ingredients/${strIngredient}-Medium.png`;
    return (
        <Item>
            <Item.Image size={imageSize} src={imageSrc}/>
            <Item.Content>
                <Item.Header>{strIngredient}</Item.Header>
                <Item.Meta>
                    <span>{strType}</span>
                    <span>{strAlcohol === 'Yes' ? 'Alcoholic' : 'Non alcoholic'}</span>
                    <span>{strABV}</span>
                </Item.Meta>
                <Item.Description>{strDescription}</Item.Description>
            </Item.Content>
        </Item>
    );
}

export default IngredientDetailItem;
