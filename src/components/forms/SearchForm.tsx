import React from 'react';
import {Form, Input} from "semantic-ui-react";
import {useForm} from "react-hook-form";
import styled from "styled-components";

const Button = styled.button`
  border: none;
  background-color: #e8e8e8;
  border-top-right-radius: 3px;
  border-bottom-right-radius: 3px;
  cursor: pointer;
`

type SearchFormProps = {
    onHandleSubmit: (name: string) => void;
    loading: boolean;
    error: boolean;
}

const SearchForm = ({onHandleSubmit, loading, error}: SearchFormProps) => {
    const {register, handleSubmit} = useForm();

    return (
        <Form onSubmit={handleSubmit((data)=> {onHandleSubmit(data.name)})}>
            <Input type='text' placeholder='Search...' action>
                <input name="name" ref={register}/>
                <Button type="submit">Search</Button>
            </Input>
        </Form>
    );
};

export default SearchForm;
