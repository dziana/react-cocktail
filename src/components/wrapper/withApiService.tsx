import React, {useContext} from "react";

import {ApiContext} from "../../contexts/ApiContext";
import {ApiService} from "../../services/apiService";

interface ComponentWithApi{
    apiService: ApiService
}

function withApiService<T extends ComponentWithApi>(WrappedComponent: React.ComponentType<T>) {
    return function (props: any) {
        const apiService = useContext(ApiContext);
        return <WrappedComponent apiService={apiService} {...props} />
    }
}

export default withApiService
