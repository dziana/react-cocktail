import React, {useContext} from "react";

import {ColorContext} from "../../contexts/ColorContext";
import {Color} from "../../models/Color.model";

interface ComponentWithApi{
    colors: Color[]
}

function withColors<T extends ComponentWithApi>(WrappedComponent: React.ComponentType<T>) {
    return function (props: any) {
        const colors = useContext(ColorContext);
        return <WrappedComponent colors={colors} {...props} />
    }
}

export default withColors
