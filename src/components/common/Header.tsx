import React from 'react';
import {withRouter} from 'react-router-dom';
import {Menu} from "semantic-ui-react";

const getCurrentPath = (pathname: string) => {
    return pathname.match(/^\/([A-Za-z]*)/)
}

const Header = ({location, history}: any) => {
    const url = getCurrentPath(location.pathname)![1];
    return (
        <Menu inverted>
            <Menu.Item
                name='home'
                active={url === ''}
                onClick={() => history.push('/')}
            />
            <Menu.Item
                name='drinks'
                active={url === 'drinks'}
                onClick={() => history.push('/drinks')}
            />
            <Menu.Item
                name='ingredients'
                active={url === 'ingredients'}
                onClick={() => history.push('/ingredients')}
            />
        </Menu>
    );
}

export default withRouter(Header);
